STEP1:
Request: http://api.openweathermap.org/data/2.5/forecast/daily?APPID=e4e1b274761e8b2ea548ca322d0c154f&q=New York,us&cnt=1

STEP2:

Created the Collection:

https://documenter.getpostman.com/view/6767352/S11Huz3Z#fec90335-d43b-4f02-aaca-bed8ae4f615d

STEP3:
Added a test on the max_temperature: if max_temperature > 10degree Celsius, the test should fail

Below are the conditional code for the test

pm.test("response is ok", function () {
    pm.response.to.have.status(200);
  });

// condition on the max_temperature: if max_temperature > 10degree Celsius, the test should fail
pm.test("environment to be production", function () { 
    //.expect(pm.environment.get("env")).to.equal("production"); 
        pm.expect(pm.response.json().list[0].temp.max).to.be.below(283.16); // converted celcius to Kelvin i.e. 283.16 kelvin =10.01 celcius
});


pm.test("response must be valid and have a body", function () {
     // assert that the status code is 200
     pm.response.to.be.ok; // info, success, redirection, clientError,  serverError, are other variants
     // assert that the response has a valid JSON body
     pm.response.to.be.withBody;
     pm.response.to.be.json; // this assertion also checks if a body  exists, so the above check is not needed
});


RESPONSE:
https://www.getpostman.com/collections/c91145008f86f7b793bd
