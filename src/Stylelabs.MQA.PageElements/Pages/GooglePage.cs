﻿using NUnit.Framework;
using OpenQA.Selenium;
using Stylelabs.MQA.Core;


namespace Stylelabs.MQA.PageElements.Pages
{
    public class GooglePage : PageBase
    {
        private static readonly string _homePageUrl = "https://www.google.com";
        private static string _searchtxtID = "lst-ib";
        private static string _resultStatsID = "resultStats";

        public GooglePage() : base(_homePageUrl) { }

        public void insertTextInSearchBox(string searchValue)
        {
            IWebElement _searchBoxLocator = Base.Driver.FindElement(By.Id(_searchtxtID));
            _searchBoxLocator.SendKeys(searchValue);
        }

        public void clickEnterKeyInSearchBox ()
        {
            IWebElement _searchBoxLocator = Base.Driver.FindElement(By.Id(_searchtxtID));
            _searchBoxLocator.SendKeys(Keys.Enter);
            Base.Driver.Manage().Timeouts().ImplicitWait = System.TimeSpan.FromSeconds(2);
         }

        public void assertResultStatIsDisplayed()
        {
            IWebElement _resultStatsLocator = Base.Driver.FindElement(By.Id(_resultStatsID));
            Assert.IsTrue(_resultStatsLocator.Text.Contains("resultsss"),"I'm not on the result page");
        }
    }
}
